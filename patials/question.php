<?php

include_once "../models/Question.php";

$Question = new Question();
$questions = $Question->findByUnit($_GET["unit"]);

?>
<table class="table table-striped">
    <thead>
        <tr>
            <th class="col-lg-1">No.</th>
            <th class="col-lg-4">Short</th>
            <th class="col-lg-2">Status</th>
        </tr>
    </thead>
    <tbody>

<?php foreach($questions as $value): ?>

   <tr>
     <td><?php echo ++$i ?></td>
     <td><?php echo $value->content ?></td>
     <td>
     <?php if ($value->status == 1):?>
        Active
        <a class="btn btn-danger btn-sm pull-right button-margin-left" type="button" href="/update_question_status.php?id=<?php echo $value->id?>&status=0">Deactive</a>
     <?php else:?>
        Uactive
        <a class="btn btn-success btn-sm pull-right button-margin-left" type="button" href="/update_question_status.php?id=<?php echo $value->id?>&status=1">Active</a>
     <?php endif; ?>
       <a class="btn btn-primary btn-sm pull-right" type="button" href="/edit_question.php?id=<?php echo $value->id ?>">Edit</a>
     </td>
   </tr>

<?php endforeach; ?>
   </tbody>
</table>


