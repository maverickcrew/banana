
<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$admin = $User->isSignin($token);
if (!$admin) {
  header("Location: signin.php");
  die();
}
$user = $User->getUserFromId($_GET["id"]);
if (!$user) {
  header ("Location: users.php");
  die();
}

?>



<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>

<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $admin->name?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>

        </div>
      </div>
    </div>

   <form class="form-signup" role="form" method="post" action="/save_user.php">
       <h2 class="form-signin-heading text-center">Edit profile</h2>
       <h3>User id: <?php echo $user->id ?></h3>
       <input name="id" type="hidden" value="<?php echo $user->id ?>"><br/>
       <input name="name" value="<?php echo $user->name ?>" type="text" class="form-control" placeholder="Name" required autofocus><br/>
       <input name="email" value="<?php echo $user->email?>" type="email" class="form-control" placeholder="Email address" required autofocus><br/>
       <input name="address" value="<?php echo $user->address?>"  type="text" class="form-control" placeholder="Address" required autofocus><br/>
        <input name="password" type="password" class="form-control" placeholder="Password" required><br/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Update</button>
   </form>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<?php include("app/layout/footer.php");?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>

 </body>

