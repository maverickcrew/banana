<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
  header("Location: index.php");
  die();
}

?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>
<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>

        </div>
      </div>
    </div>

<a href="/add_an_unit.php" class="pull-left btn btn-success">Add An Unit</a>
<a href="/add_an_occupation.php" class="pull-left btn btn-success button-margin-left">Add An Occupation</a>
<a href="/add_a_question.php" class="pull-left btn btn-success button-margin-left">Add A Question</a>

<select id="unit" class="input-sm pull-right button-margin-left col-lg-2">
  <option value="">Please select one</option>
</select>
<?php
include_once "models/Category.php";
 $Category = new Category();
 $categories = $Category->getAll();
?>

<select id="occupation" class="input-sm pull-right col-lg-2">
  <option value="" selected="true">Please select one</option>
  <?php foreach($categories as $key): ?>
    <option value=<?php echo $key->id ?>  ><?php echo $key->occupation ?></option>
  <?php endforeach; ?>
</select>

<div class="header">
<h1 id="occupationname"></h1>
<a id="editoccupation" style="display: none;" class="btn btn-primary" href="#">Edit</a>
</div>

<div id="units">
</div>

<br/>

<div id="question">
</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>

<script>
$(document).ready(function(){

  $("#occupation").change(function(){
    if ($("#occupation").find(":selected").val()) {
      $("#occupationname").text($("#occupation").find(":selected").text());
      $("#editoccupation").prop("href", "/edit_occupation.php?id=" + $(this).val());
      $("#editoccupation").show();
    } else  {
      $("#editoccupation").hide();
      $("#occupationname").text("");
    }
    $("#unit").load("/patials/unit.php?occupation=" + $(this).find(":selected").val());

    $("#units").load("/patials/units.php?occupation=" + $(this).find(":selected").val());
  });

  $("#unit").change(function(){
    $("#question").load("/patials/question.php?unit=" + $(this).find(":selected").val());
  });

  $("#category").load("/patials/category.php");

});

</script>

 </body>

