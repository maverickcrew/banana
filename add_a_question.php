<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
  header("Location: index.php");
  die();
}

?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>
<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>

        </div>
      </div>
    </div>

<select id="unit" class="input-sm pull-right button-margin-left col-lg-2">
  <option value="">Please select one</option>
</select>

<?php
include_once "models/Category.php";
 $Category = new Category();
 $categories = $Category->getAll();
?>

<select id="occupation" class="input-sm pull-right col-lg-2">
  <option value="" selected="true">Please select one</option>
  <?php foreach($categories as $key): ?>
    <option value=<?php echo $key->id ?>  ><?php echo $key->occupation ?></option>
  <?php endforeach; ?>
</select>

<form id="theform" class="form-signup" role="form" method="post" action="/submit_question.php">
       <h2 class="form-signin-heading text-center">Question</h2>
        <textarea placeholder="Question" name="content" class="form-control" rows="4" cols="50"></textarea><br/>
        <input name="caseA" type="text" class="form-control" placeholder="Case A" required><br/>
        <input name="caseB" type="text" class="form-control" placeholder="Case B" required><br/>
        <input name="caseC" type="text" class="form-control" placeholder="Case C" ><br/>
        <input name="caseD" type="text" class="form-control" placeholder="Case D" ><br/>

        <select id="answer" name="answer" class="input-sm pull-right col-lg-4">
              <option value="" selected="true">Please select one</option>
              <option value="0">Case A</option>
              <option value="1">Case B</option>
              <option value="2">Case C</option>
              <option value="3">Case D</option>
        </select><br/><br/><br/>
        <input name="unit" id="unitid" type="hidden" value="" >
        <input name="status" id="unitid" type="hidden" value="1" >
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
        <label id="occupationerror"></label><br/>
        <label id="uniterror"></label><br/>
        <label id="answererror"></label><br/>
</form>

<br/>
<br/>
<br/>

<?php include("app/layout/footer.php");?>
</div><!-- /.container -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>
<script>
$(document).ready(function(){

  $("#theform").bind("submit", function(e){
    if ($("#occupation").find(":selected").val()=="") {
       $("#occupationerror").show();
       $("#occupationerror").text("Hãy chọn một occupation");
       e.preventDefault();
    } else  {
        $("#occupationerror").hide();
    }
    if ($("#unit").find(":selected").val()=="") {
       $("#uniterror").show();
       $("#uniterror").text("Hãy chọn một unit");
       e.preventDefault();
    } else  {
        $("#uniterror").hide();
    }
    if ($("#answer").find(":selected").val()=="") {
       $("#answererror").show();
       $("#answererror").text("Hãy chọn một answer");
       e.preventDefault();
    } else  {
        $("#answererror").hide();
    }


  });

  $("#occupation").change(function(){
    $("#unitid").val("");
    $("#unit").load("/patials/unit.php?occupation=" + $(this).find(":selected").val());
  });
  $("#unit").change(function(){
    $("#unitid").val($(this).find(":selected").val());
  });
});

</script>
 </body>

