<?php

include_once "models/Unit.php";
include_once "models/User.php";

$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
  header("Location: index.php");
  die();
}

$Unit = new Unit();
$occupation = $_POST["occupation"];
if (!$occupation) {
  header("Location: add_an_unit.php");
  die();
}

$Unit->insert($_POST);

header("Location: question.php");
die();

?>
