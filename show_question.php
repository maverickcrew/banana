<?php

include_once "models/Question.php";
include_once "models/User.php";

$question_id = $_GET["id"];
$Question = new Question();
$question = $Question->getOne($question_id);
if (!$question) {
  header("Location: index.php");
  die();
}
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isSignin($token);


?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>
<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
         <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
         <input name="q" type="text" class="form-control" value="<?php echo $_GET["q"]?>" placeholder="Search...">
          </form>
        </div>
      </div>
    </div>

<div id="theform" class="form-signup">
       <h2 class="form-signin-heading text-center">Question</h2>
        <label><?php echo $question->content ?></label><br/>
       <h2 class="form-signin-heading text-center">Cases</h2>
        <label>A.<?php echo $question->caseA ?></label><br/>
        <label>B.<?php echo $question->caseB ?></label><br/>
        <label>C.<?php echo $question->caseC ?></label><br/>
        <label>D.<?php echo $question->caseD ?></label><br/>
</div>

<br/>
<br/>
<br/>

<?php include("app/layout/footer.php");?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>

 </body>
