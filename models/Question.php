<?php

include_once "connection/Table.php";

class Question extends Table {

  public function count() {
    $select = $this->select()->where("`status` = 1");
    $result = $this->fetchOne($select->getCountQuery());
    return $result->{'COUNT(*)'};
  }

  public function countWithUnit($unit = nul) {
    $select = $this->select()->where("`unit`= $unit AND `status`=1");
    $result = $this->fetchOne($select->getCountQuery());
    return $result->{'COUNT(*)'};
  }

  public function getOne($id = null) {

    if ($id) {
      $select = $this->select(array("id", "content", "unit", "caseA", "caseB", "caseC", "caseD", "answer", "status"))->where("`id` = '$id'");
      return $this->fetchOne($select);
    }
    return null;

  }

  public function findByUnit($unit = null) {
    if ($unit) {
      $select = $this->select(array("id","content", "status", "unit", "caseA", "caseB", "caseC", "caseD", "answer"))->where("`unit` = $unit");
      return $this->fetchAll($select);
    }
    return null;
  }

  public function findByUnitWithOffset($unit = null, $offset = 0) {
    if ($unit) {
      $select = $this->select(array("id","content", "unit", "caseA", "caseB", "caseC", "caseD", "answer"))->where("`unit` = $unit AND `status`=1")->limit(1, $offset);
      return $this->fetchOne($select);
    }
    return null;
  }

  public function findByKey($key = null) {
    if ($key) {
      $select = $this->select(array("id","content", "unit", "caseA", "caseB", "caseC", "caseD", "answer"))->where("`content` LIKE \"%$key%\"");
      return $this->fetchAll($select);
    }
    return null;
  }

  public function isCorrect($unit, $offset, $key) {
    $answer = $this->findByUnitWithOffset($unit, $offset)->answer;
    return ($key == $answer);
  }

}

?>
