<?php
include_once "models/User.php";
include_once "models/Question.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isSignin($token);

if (!$user) {
    header("Location: signin.php");
      die();
}
$Question = new Question();
$question = $Question->getOne($_GET["id"]);
if (!$question) {
  header("Location: index.php");
  die();
}

$question->status = $_GET["status"];

$Question->update($question);
header("Location: question.php");
die();
?>
