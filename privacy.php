<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isSignin($token);

?>
<?php include("app/layout/header.php");?>
<body>
<div class="navbar-wrapper">
  <div class="container">

    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
            </ul>
            </li>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>

        </div>
      </div>
    </div>

<h1>CoreDump Privacy Policy</h1>
<p>Most importantly: we will not sell, trade, give, etc., the statistics that we collect on your site to any third party, without your express permission. The data that we collect is not mined or used for marketing purposes in any way. The data that we collect is used solely for purposes of web metrics and traffic analysis by Opentracker.net. Be assured that safeguarding the contents of your website traffic is our priority.</p>

<p>banana.lozi.vn, is committed to safeguarding your privacy on-line. The following statement sets our privacy policy. For the purposes of this privacy statement "banana.lozi.vn" shall mean banana.lozi.vn. This policy may change from time to time so please re-read this policy periodically.</p>

<h3>what we gather from you</h3>
<p>Our site collects information in several ways. When you order information, registrations and services from us you are required to complete a client order form. banana.lozi.vn requests your name, e-mail address and your contact information ("Customer Information").</p>

<p>As part of our service, banana.lozi.vn also uses cookies to help identify you and access your account data, to track you, to track traffic patterns and for system administration. A cookie is a small text file that is created by your browser from a web page and is stored on your computer's hard drive.</p>

<h3>contact information</h3>
<p>Our site's registration form requires users to give us contact information (like email address). Contact information is used to get in touch with them when necessary. </p>

<h3>security</h3>
<p>This site has security measures in place to protect against the loss, misuse and alteration of the information under our control. A password must be entered to log in to your account and in order to manage your domain name(s). All personal information is fully encrypted during all transactions. Please note, however, that no data transmission over the Internet is completely secure. As a result, while we endeavour to protect your personal information, we cannot ensure or warrant the security of any information you provide to us and you transmit such information to us at your own risk. Once we receive your transmission we endeavour to ensure the security of your personal information on our systems. Please note that Opentracker.net will never ask for any personal information via email (e.g. marriage status, income, etc.).</p>
<h3>correct/update</h3>
<p>If you wish to correct, update or otherwise modify the information that you have provided to us please visit the Profile section on our web site and correct, update or modify the relevant information.</p>
<h3>contacting the web site</h3>
<p>If you have any questions about this privacy statement, the practices of this site or your dealings with this Web site, please e-mail&nbsp;<a href="mailto:coredumpgroup@gmail.com">coredumpgroup@gmail.com</a></p>
<h3>links</h3>
<p>This web site contains links to other web sites. Please note that when you click on one of these links, you are 'clicking' to another web site. We encourage you to read the privacy statements of these linked sites as their privacy policy may differ from ours.</p>
<br>
<br>
<br>
<?php include("app/layout/footer.php");?>
  </div>
</div>
<br>
<br>
<br>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>

</body>
