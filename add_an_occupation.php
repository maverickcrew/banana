<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
  header("Location: signin.php");
  die();
}

?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>
<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>

        </div>
      </div>
    </div>


<form id="theform" class="form-signup" role="form" method="post" action="/submit_occupation.php">
   <h2 class="form-signin-heading text-center">New Occupation</h2>
     <input id="name" type="text" placeholder="Name" name="occupation" class="form-control"></input><br/>
     <textarea id="description" name="description" class="form-control" rows="6" placeholder="Description"></textarea><br>
     <button class="btn btn-lg btn-primary col-lg-5" type="submit">Submit</button>
     <a href="/question.php" class="btn btn-lg btn-primary col-lg-5 pull-right">Cancel</a>
     <label id="nameerror"></label>
     <label id="descriptionerror"></label><br/>
</form>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

<?php include("app/layout/footer.php");?>
</div><!-- /.container -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>
<script>
$(document).ready(function(){

  $("#theform").bind("submit", function(e){
  if ($("#name").val()=="") {
      $("#nameerror").show();
      $("#nameerror").text("Occupation name không được bỏ trống");
      e.preventDefault();
  } else  {
      $("#nameerror").hide();
  }
  if ($("#description").val()=="") {
      $("#descriptionerror").show();
      $("#descriptionerror").text("Description không được bỏ trống");
      e.preventDefault();
  } else  {
      $("#descriptionerror").hide();
  }
 });

  $("#occupation").change(function(){
    $("#occupationid").val($(this).find(":selected").val());
  });
});

</script>
 </body>

