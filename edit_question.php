<?php include_once "models/User.php";
include_once "models/Question.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
  header("Location: index.php");
  die();
}

$question_id = $_GET['id'];
$Question = new Question();
$question = $Question->getOne($question_id);
if (!$question) {
  header("Location: question.php");
  die();
}

?>

 <?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>
<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>

        </div>
      </div>
    </div>

<form id="theform" class="form-signup" role="form" method="post" action="/save_question.php">
       <h2 class="form-signin-heading text-center">Question</h2>
        <textarea name="content" class="form-control" rows="4" cols="50" required><?php echo $question->content ?></textarea><br/>
        <input name="caseA" type="text" class="form-control" placeholder="Case A" required value="<?php echo $question->caseA ?>"><br/>
        <input name="caseB" type="text" class="form-control" placeholder="Case B" required value="<?php echo $question->caseB ?>"><br/>
        <input name="caseC" type="text" class="form-control" placeholder="Case C" value="<?php echo $question->caseC ?>"><br/>
        <input name="caseD" type="text" class="form-control" placeholder="Case D" value="<?php echo $question->caseD ?>"><br/>
        <select id="answer" name="answer" class="input-sm pull-right col-lg-4">
              <option value="">Please select one</option>
              <option value="0" <?php if ($question->answer == 0) { echo "selected=\"selected\""; } ?> >Case A</option>
              <option value="1" <?php if ($question->answer == 1) { echo "selected=\"selected\""; } ?> >Case B</option>
              <option value="2" <?php if ($question->answer == 2) { echo "selected=\"selected\""; } ?> >Case C</option>
              <option value="3" <?php if ($question->answer == 3) { echo "selected=\"selected\""; } ?> >Case D</option>
        </select><br/><br/><br/>
        <input name="unit" id="unitid" type="hidden" value="" >
        <input name="status" id="unitid" type="hidden" value="1" >
        <input name="id" type="hidden" value="<?php echo $question->id ?>" >
        <button class="btn btn-lg btn-primary col-lg-5" type="submit">Submit</button>
        <a href="/question.php" class="btn btn-lg btn-primary col-lg-5 pull-right">Cancel</a>
        <label id="answererror"></label><br/>
</form>

<br/>
<br/>
<br/>

<?php include("app/layout/footer.php");?>
</div><!-- /.container -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>

<script>
$("#theform").bind("submit", function(e){
  if ($("#answer").find(":selected").val() == "") {
      $("#answererror").show();
      $("#answererror").text("Hãy chọn đáp án cho câu hỏi này");
      e.preventDefault();
  } else  {
      $("#answererror").hide();
  }
});
</script>

 </body>
