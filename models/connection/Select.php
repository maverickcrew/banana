<?php

class Select {

  protected $_columns, $_table, $_joins, $_conditions, $_groups, $_orderings, $_limit, $_offset;

  public function __construct() {
    $this->_columns = array();
    $this->_table = '';
    $this->_joins = array();
    $this->_conditions = array();
    $this->_groups = array();
    $this->_orderings = array();
    $this->_limit = null;
    $this->_offset = 0;
  }

  public function getQuery() {
    // SELECT
    $query = 'SELECT ';
    if (count($this->_columns) > 0) {
      foreach ($this->_columns as $column) {
        $query .= " $column, ";
      }
      $query = substr($query, 0, -2);
    } else {
      $query .= ' * ';
    }


    // FROM
    $query .= ' FROM ' . $this->_table . ' ';

    // JOIN
    foreach ($this->_joins as $join) {
      $query .= " JOIN $join ";
    }

    // WHERE
    if (count($this->_conditions) > 0) {
      $query .= ' WHERE ';
      foreach ($this->_conditions as $condition) {
        $query .= " ($condition) AND ";
      }
      $query = substr($query, 0, -4);
    }

    // GROUP BY
    if (count($this->_groups) > 0) {
      $query .= ' GROUP BY ';
      foreach ($this->_groups as $group) {
        $query .= " $group, ";
      }
      $query = substr($query, 0, -2);
    }

    // ORDER BY
    if (count($this->_orderings) > 0) {
      $query .= ' ORDER BY ';
      foreach ($this->_orderings as $ordering) {
        $query .= "$ordering, ";
      }
      $query = substr($query, 0, -2);
    }

    // LIMIT
    if (isset($this->_limit)) {
      $query .= ' LIMIT ' . $this->_offset . ', ' . $this->_limit;
    }

    return $query;
  }

  public function getCountQuery() {
    // SELECT
    $query = 'SELECT COUNT(*) FROM ' . $this->_table . ' ';

    // WHERE
    if (count($this->_conditions) > 0) {
      $query .= ' WHERE ';
      foreach ($this->_conditions as $condition) {
        $query .= "($condition) AND ";
      }
      $query = substr($query, 0, -4);
    }

    return $query;
  }

  public function select($fields) {
    if (!is_array($fields)) {
      $fields = array($fields);
    }

    foreach ($fields as $field) {
      if (!in_array($field, $this->_columns)) {
        $this->_columns[] = $field;
      }
    }

    return $this;
  }

  public function from($table) {
    $this->_table = $table;

    return $this;
  }

  public function join($table, $condition) {
    $string = "$table ON $condition";
    if (!in_array($string, $this->_joins)) {
      $this->_joins[] = $string;
    }

    return $this;
  }

  public function where($conditions) {
    if (!is_array($conditions)) {
      $conditions = array($conditions);
    }

    $condition = '';
    foreach ($conditions as $c) {
      $condition .= "($c) OR";
    }
    $condition = substr($condition, 0, -3);

    $this->_conditions[] = $condition;

    return $this;
  }

  public function group($fields) {
    if (!is_array($fields)) {
      $fields = array($fields);
    }

    foreach ($fields as $field) {
      if (!in_array($field, $this->_groups)) {
        $this->_groups[] = $field;
      }
    }

    return $this;
  }

  public function order($fields) {
    if (!is_array($fields)) {
      $fields = array($fields);
    }

    foreach ($fields as $field) {
      if (!in_array($field, $this->_orderings)) {
        $this->_orderings[] = $field;
      }
    }

    return $this;
  }

  public function limit($limit, $offset = 0) {
    $this->_limit = $limit;
    $this->_offset = $offset;

    return $this;
  }

}

?>
