<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
if ($User->isSignin($token)) {
  header("Location: index.php");
  die();
}

?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>
<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <li><a href="/signin.php">Signin</a></li>
          </ul>
        </div>
      </div>
    </div>

   <form class="form-signin" role="form" method="post" action="/session.php">
       <h2 class="form-signin-heading text-center">Sign in</h2>
        <input name="email" type="email" class="form-control" placeholder="Email address" required autofocus>
        <input name="password" type="password" class="form-control" placeholder="Password" required>
        <label class="checkbox">
          <input name="remember" type="checkbox" value="remember-me">Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button><br/>
           <a href="/register.php">You don't have an account?</a>
   </form>
</div>

 </body>

