<?php

include_once "../models/Unit.php";

$Unit = new Unit();
$units = $Unit->findByOccupation($_GET["occupation"]);

?>
<table class="table table-striped">
    <thead>
        <tr>
            <th class="col-lg-1">No.</th>
            <th class="col-lg-6">Unit</th>
        </tr>
    </thead>
    <tbody>

   <?php foreach($units as $value): ?>
   <tr>
     <td><?php echo ++$i ?></td>
     <td><?php echo $value->name ?><a class="btn btn-primary btn-sm pull-right button-margin-left" type="button" href="/edit_unit.php?id=<?php echo $value->id?>">Edit</a>
     </td>
   </tr>
   <?php endforeach; ?>
   </tbody>
</table>
