<?php

include_once "Mysql.php";
include_once "Row.php";
include_once "Select.php";
define("MAX_INSERTED_ROWS", 2000);


class Table {

  protected $_connection;
  protected $_db_name, $_name, $_columns, $_model_row;

  public function __construct() {

    $this->_connection = Mysql::open("localhost", "3306", "root", "", "banana", true);
    $class_name = get_class($this);
    $this->_name = $class_name;
    $this->_model_row = "Row";
    $this->getAllColumns();

  }

  public function fetchAll($select = null) {
    if (!isset($select)) {
      $select = $this->select();
    }

    $query = is_object($select) ? $select->getQuery() : $select;

    $rows = array();

    $result = $this->query($query);

    if ($result) {
      while ($item = mysql_fetch_assoc($result)) {
        $row = new $this->_model_row;

        foreach ($item as $column => $value) {
          $row->{$column} = $value;
        }

        if (isset($row->id)) {
          $rows[$row->id] = $row;
        } else {
          $rows[] = $row;
        }
      }
    }
    return $rows;
  }

  protected function query($query) {
    if (!isset($this->_connection)) {
      $this->_connection = Mysql::open();
    }

    $result = mysql_query($query, $this->_connection);

    if ($result == null) {
      echo $error = "MySQL QUERY: $query\nMySQL ERROR: " . mysql_error($this->_connection) . "\n";
    }

    return $result;
  }

  protected function getAllColumns() {
    $this->_columns = new stdClass();
    $rows = $this->fetchAll("SHOW COLUMNS FROM ". $this->_name);
    foreach ($rows as $row) {
      $this->_columns->{$row->Field} = $row->Type;
    }
  }


  public function select($columns = null) {
    $select = new Select();

    if (isset($columns)) {
      $select->select($columns);
    }

    $select->from($this->_name);

    return $select;
  }

  public function insert($rows) {
    if (!isset($this->_connection)) {
      $this->_connection = Libraries_Database_Connection::open();
    }

    if (!is_array($rows) || (count($rows) === 0)) {
      return false;
    }

    // If $rows is only one row
    if (!is_array(current($rows)) && !is_object(current($rows))) {
      $rows = array($rows);
    }

    // Build query
    $query_fields = "INSERT IGNORE INTO {$this->_name} (";
    foreach ($this->_columns as $column => $type) {
      $query_fields .= "`$column`,";
    }
    $query_fields = substr($query_fields, 0, -1) . ') VALUES ';

    $number_rows = 0;
    $query = $query_fields;
    foreach ($rows as $row) {
      $number_rows++;

      // If row is object, convert it to array
      if (is_object($row)) {
        $row = (array)$row;
      }

      $query .= '(';
      foreach ($this->_columns as $column => $type) {
        $query .= isset($row[$column]) ? "'" . mysql_real_escape_string($row[$column]) . "'," : 'NULL,';
      }
      $query = substr($query, 0, -1) . '),';

      if ($number_rows % MAX_INSERTED_ROWS == 0) { // Insert every MAX_INSERTED_ROWS rows
        $query = substr($query, 0, -1);
        $this->query($query);
        $query = $query_fields;
      }
    }
    if ($number_rows % MAX_INSERTED_ROWS != 0) { // Insert the rest rows
      $query = substr($query, 0, -1);
      $this->query($query);
    }
  }

  public function update($fields, $conditions = null) {
    if (!isset($this->_connection)) {
      $this->_connection = Mysql::open();
    }

    if (is_object($fields)) {
      $fields = (array)$fields;
    }

    $query = 'UPDATE ' . $this->_name . ' SET ';
    foreach ($this->_columns as $colunm => $type) {
      if (isset($fields[$colunm]) && ($fields[$colunm] != 'id')) {
        $query .= " $colunm = '" . (isset($fields[$colunm]) ? mysql_real_escape_string($fields[$colunm]) : NULL) . "', ";
      }
    }
    $query = substr($query, 0, -2) . " WHERE ";

    // If no condition
    if (!isset($conditions)) {
      $conditions = "id = {$fields['id']}";
    }
    // If $rows is only one row
    if (!is_array($conditions)) {
      $conditions = array($conditions);
    }

    foreach ($conditions as $condition) {
      $query .= " ($condition) AND ";
    }
    $query = substr($query, 0, -4);

    return $this->query($query);
  }


  public function fetchOne($select) {
    $query = is_object($select) ? $select->getQuery() : $select;

    $result = $this->query($query);

    if ($result) {
      while ($item = mysql_fetch_assoc($result)) {
        $row = new $this->_model_row;
        foreach ($item as $column => $value) {
          $row->{$column} = $value;
        }
        return $row;
      }
    }
    return null;

  }

}
?>
