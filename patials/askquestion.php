<?php

include_once "../models/Question.php";

$Question = new Question();
$unit = $_GET["unit"];
$current = $_GET["current"];
$question = $Question->findByUnitWithOffset($unit, $current);

?>

<?php if ($question): ?>


<h1 class="cover-heading">Câu <?php echo $_GET["current"] + 1 ?></h1>
<p class="lead"><?php echo $question->content ?></p>
            <p class="lead">
<hr/>
<div class="radio text-left">
  <label>
    <input type="radio" name="answer" id="answer" value="0">
    <?php echo $question->caseA ?>
  </label>
</div>
<div class="radio text-left">
  <label>
    <input type="radio" name="answer" id="answer" value="1">
    <?php echo $question->caseB ?>
  </label>
</div>

<div class="radio text-left">
  <label>
    <input type="radio" name="answer" id="answer" value="2">
    <?php echo $question->caseC ?>
  </label>
</div>
<div class="radio text-left">
<label>
    <input type="radio" name="answer" id="answer" value="3">
    <?php echo $question->caseD ?>
</label>
</div>
<a type="hidden" id="questionid" value="<?php echo $question->id ?>">
<?php else: ?>
<h3>
<?php

$answersheet = $_GET["answersheet"];
$scrore = 0;
for($i = 0; $i< count($answersheet); $i++ ) {

  if ($Question->isCorrect($unit, $i, $answersheet[$i])) {
     $scrore++;
  }

}
echo "Chúc mừng bạn đã hoàn thành bài test, điểm của bạn là ";
echo $scrore;
echo "/";
echo count($answersheet);

?>
</h3>
<?php endif ?>
