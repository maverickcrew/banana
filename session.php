<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];

if ($User->isSignin($token)) {
  header("Location: index.php");
  die();
}

$email = $_POST["email"];
$user = $User->findByEmail($email);

if ($user) {
  if ($user->password == $_POST["password"]) {
    $token = uniqid(mt_rand(), true);
    $expire=time()+60*60*24*30;
    setcookie("token", $token, $expire);
    $user->token = $token;
    $User->update($user);
    header("Location: index.php");
    die();
  }
}

header("Location: signin.php");
die();

?>
