<?php
include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isSignin($token);
?>
<?php include("app/layout/header.php");?>
<body>
<div class="navbar-wrapper">
  <div class="container">

    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
            </ul>
            </li>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>

        </div>
      </div>
    </div>

<h1>Terms of Agreement</h1>

<p>This banana.lozi.vn Membership Agreement (the "Agreement") is a legal contract between you and banana.lozi.vn, ("banana.lozi.vn").</p>
<p>The Agreement governs your purchase and use of the online website measurements service, whether fee-based or free of charge, offered on banana.lozi.vn's website (each a "Service"). Your use of the Services is conditioned on your acceptance of this Agreement. By using any Service or by checking the acceptance box at the sign-up page, you accept and agree to be bound by all the terms and conditions of this Agreement, as well as any additional terms specific to the particular Services for which you register.</p>
<p>Since this is a binding legal agreement between you and banana.lozi.vn, please print a copy of this Agreement for your records.</p>
<h3>1. the services</h3>
<p>banana.lozi.vn will provide you with the Services for which you register on banana.lozi.vn's website, subject to the terms and conditions of this Agreement. With respect to free-of-charge 4-week-trial, the fees referred to in this Agreement will be equal to zero dollars ($ 0.00). Descriptions of the Service, are available on banana.lozi.vn's website.</p>
<p>banana.lozi.vn reserves the right to modify, enhance, or supplement the Services at any time in its sole discretion, provided that those modifications do not materially and adversely affect your rights or obligations under this Agreement.</p>
<h3>2. registration process</h3>
<p>To sign up for or receive any Service, you must submit or maintain on file with banana.lozi.vn certain registration data, as requested by banana.lozi.vn's online registration form for the Service. Such registration data shall include, but not be limited to, your name, address, email address, website URLs, credit card number, and other billing information. (However: Opentracker itself does not store, hold, process, or access your credit card information. That transaction is handled in its entirety by our 3rd party secure processor: PayPal). banana.lozi.vn reserves the right, in its sole discretion, to refuse any registration or Service activation request for any reason or no reason, and shall not be obligated to provide any Service to you unless and until your credit card has been charged for the applicable fee. banana.lozi.vn's use of your registration information is governed by banana.lozi.vn's privacy policy.</p>
<h3>3. your promises and obligations as a condition of receiving any services, you promise, represent, and warrant the following.</h3>
<ol><li>All of the registration information you supply to banana.lozi.vn is true, complete, and accurate, and you will notify banana.lozi.vn of any changes to your registration data during the term of this Agreement and submit updated information within twenty (20) days of any such changes;</li>
<li>None of the URLs you submit to banana.lozi.vn link to any web page or site that contains any:
<ol><li>hate propaganda,</li>
<li>material that encourages or promotes illegal activity or violence;</li>
<li>content that violates or infringes in any way upon the statutory, common law, or</li>
<li>proprietary rights of others, including but not limited to copyrights, trademark rights,</li>
<li>patents, or any other third party intellectual property, contract, privacy, or publicity rights;</li>
<li>material that promotes or utilizes software or services designed to deliver unsolicited email;</li>
<li>material that violates any local, state, or national law or regulation;</li>
<li>misrepresentations or material that is threatening, abusive, harassing, libelous, or defamatory,</li>
<li>obscene, profane, indecent, or otherwise objectionable, offensive, or harmful; or</li>
<li>other material that banana.lozi.vn, in its sole discretion, deems inappropriate, including any violations of standards posted on banana.lozi.vn's website or sent to you by email;</li>
</ol></li>
<li>You will safeguard your account user name and password by not disclosing your password to any third party, and you will assume responsibility for any and all harm or liability attributable to you or any other person accessing your account or any Services with your user name and password;</li>
<li>You will not copy, sell, redistribute, license, sublicense, or otherwise transfer your account, or any materials provided to you in connection with the Services, to any third party without banana.lozi.vn's written consent;</li>
<li>You will treat any and all consumer information gathered for you or transmitted to you via the banana.lozi.vn website or Services in accordance with banana.lozi.vn's Privacy Policy;</li>
<li>You will comply with all federal laws and regulations governing your actions under this Agreement; and</li>
<li>You have full power and authority to enter into this Agreement and to perform your obligations hereunder.</li>
</ol><p>Without limiting its other remedies, banana.lozi.vn may refuse or cancel your account or Services at any time for any violation of the foregoing promises. To assure compliance with the criteria in Subsection (b) above, banana.lozi.vn reserves the right to monitor the content of the web pages or sites that correspond to the URLs you submit to banana.lozi.vn.</p>
<h3>4. payment</h3>
<p>You agree to pay banana.lozi.vn any set-up fees and recurring subscription fees listed on banana.lozi.vn's website for the service you select. Completion and submission of any Service activation form authorizes banana.lozi.vn to charge the credit card specified in your registration for all such fees and any related taxes corresponding to the selected Service. Such charges will be made on a one-time, monthly, or yearly basis. In order to avoid the full fee for any new subscription period, you must cancel the Service BEFORE the new period begins. Any failure to pay the amounts due under this Agreement will be a material breach of your obligations under this Agreement. Without limiting any other remedies available to banana.lozi.vn, YOU AGREE TO FULLY REIMBURSE banana.lozi.vn FOR ANY COSTS AND EXPENSES THAT banana.lozi.vn MAY INCUR IN CONNECTION WITH COLLECTING OR ATTEMPTING TO COLLECT ANY SUCH UNPAID AMOUNTS.</p>
<h3>5. pricing changes and other modifications</h3>
<p>The banana.lozi.vn services are based on a particular amount of page views per month. banana.lozi.vn may change its Service fees, or any other terms of this Agreement, at any time by updating the applicable pricing information or other terms posted on banana.lozi.vn's website. New pricing terms will become effective beginning with the first full billing cycle after banana.lozi.vn posts such changes to its website. Changes to terms not related to pricing will become effective fifteen (15) days after such notification. If you do not agree to any changes posted by banana.lozi.vn in accordance with this Section, you must cancel your account or the affected Service as provided herein before the given changes become effective; otherwise you will be deemed to have accepted and agreed to the changes.</p>
<p>Additional modifications to these terms and conditions may be made in special circumstances, but only if approved in advance in a signed writing on banana.lozi.vn's letterhead by an executive vice president or higher-level officer of banana.lozi.vn. Product sales and support representatives are not authorized to waive or modify any provisions of this Membership Agreement. No failure by banana.lozi.vn to enforce any term of this Agreement shall be construed as a waiver thereof, nor shall it affect your obligations or banana.lozi.vn's rights and remedies hereunder.</p>
<h3>6. duration and cancellation of services</h3>
<p>Unless stated otherwise on banana.lozi.vn's web pages applicable to a given Service, each Service will be provided and billed on a monthly or yearly subscription basis, measured from the day of payment-activation and continuing in like intervals; monthly or yearly. Subscriptions will activate regular payments with monthly or annual periods, as applicable, until cancelled in accordance with this section. At any time after activation of a Service, either you or banana.lozi.vn may cancel that Service (or banana.lozi.vn may discontinue the Service completely) by directly canceling the subscription agreement through PayPal, WorldPay, or the relevant payment processor, in which case the Service will terminate at the end of the current monthly or yearly billing period. banana.lozi.vn may cancel the service if you exceed the particular number of monthly page views per month as selected during the subscribing process. Opentracker may also increase your subscription fee according to the relevant bracket of page views, if your site's traffic consistently exceeds the bracket initially chosen for service.<br>
  banana.lozi.vn also reserves the right to cancel any individual Service immediately and without notice in the event that you breach any provision of this Agreement or any other terms that apply to that Service. NO REFUNDS WILL BE AVAILABLE ON ACCOUNT OF ANY SERVICE CANCELLATION BY EITHER PARTY, EXCEPT AS EXPRESSLY PROVIDED IN THE SECTION ENTITLED "REFUNDS" BELOW.</p>
<h3>7. term and termination of agreement</h3>
<p>The terms of this Agreement will begin on the date that banana.lozi.vn accepts your initial registration and will continue in force until terminated according to this section. Upon activation of all active Services, or any time thereafter, either party may terminate your account and this Agreement by written notice to the other party. banana.lozi.vn also reserves the right to terminate your account and this Agreement immediately and without notice in the event that you breach any provision of this Agreement. No refunds will be available on account of any termination of this Agreement, except as expressly provided in the Section entitled "Refunds" below. Sections 10 through 15 of this Agreement shall survive and remain in effect notwithstanding any termination of this Agreement. If this agreement is terminated by us or by you for any reason, you agree to remove our code, logos and trademarks and any other parts of the Service from all of your Web sites and other items. Failure to remove our code from your website entitles us to act in such a manner as to recover the additional costs generated. This manner includes, but is not limited to the placement of redirects and/ or pop-ups</p>
<h3>8. refunds</h3>
<p>If you cancel any Service you acknowledge and agree that NO REFUNDS WILL BE ISSUED.&nbsp;</p>
<h3>9. third party disclosure</h3>
<p>banana.lozi.vn will not knowingly disclose statistics pertaining to your site to a third party, without your explicit permission. In other words the statistics provided by banana.lozi.vn are strictly private and only accessible by our administrators, and the user accounts that you create.</p>
<h3>10. use of tags</h3>
<p>Some of the Services rely on the use of HTML tags or other code in your web page or site. Subject to your compliance with all the terms of this Agreement, banana.lozi.vn grants you permission to use the HTML tags or other code supplied to you by banana.lozi.vn (the "Tags") solely for your use in receiving the Services for which you have paid. You agree to follow all instructions and restrictions provided to you by banana.lozi.vn with respect to your use of the Tags. You agree that banana.lozi.vn will not be responsible for any malfunctions, errors, data inaccuracies, or improper results attributable to your incorrect, unauthorized, or unsupported use of any Tags.</p>
<h3>11. ownership rights</h3>
<p>You agree and understand that the Services and all graphic designs, icons, Tags, HTML code, computer programming, and other elements incorporated therein are the exclusive property of banana.lozi.vn. In addition, you acknowledge that banana.lozi.vn owns all right, title, and interest in and to banana.lozi.vn's trademarks, trade names, service marks, inventions, copyrights, trade secrets, patents, technology, software, and know-how related to the design, function, or operation of the Service. Your rights to the Services are strictly limited to the rights expressly granted in this Agreement.</p>
<h3>12. disclaimer of warranty</h3>
<p>banana.lozi.vn makes no guarantees of any kind regarding the use or the results derived from any Services in terms of dependability, accuracy, security, timeliness, availability, reliability, or usefulness. The Services and all related materials are provided "AS IS" without warranty of any kind. banana.lozi.vn HEREBY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, WITH REGARD TO THE SERVICES AND RELATED MATERIALS, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. banana.lozi.vn DOES NOT WARRANT THAT THE OPERATION OF THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT THE SERVICES WILL BE SUITABLE FOR YOUR NEEDS OR YOUR INTENDED APPLICATIONS, OR THAT THE SERVICES WILL BE COMPATIBLE WITH OR OPERATE IN THE HARDWARE, SOFTWARE, OR WEBSITE CONFIGURATIONS THAT YOU SELECT.</p>
<h3>13. limitation of liability</h3>
<p>IN NO EVENT WILL banana.lozi.vn BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY CONSEQUENTIAL, INCIDENTAL, SPECIAL, OR INDIRECT DAMAGES OF ANY KIND OR NATURE, INCLUDING BUT NOT LIMITED TO LOSS OF INCOME, LOSS OR DAMAGE OF DATA, AND DAMAGE TO BUSINESS REPUTATION, UNDER ANY THEORY OF LAW OR EQUITY, AND WHETHER OR NOT banana.lozi.vn IS NOTIFIED OF THE POSSIBILITY OF SUCH DAMAGES.</p>
<p>FURTHERMORE, IN TOTAL, CUMULATIVE LIABILITY UNDER THIS AGREEMENT EXCEED THE AMOUNT RECEIVED BY banana.lozi.vn FROM YOU UNDER THIS AGREEMENT. Without limiting the foregoing, banana.lozi.vn is not responsible for any of your data residing on banana.lozi.vn hardware. You are responsible for backing-up your data and information that may reside on the banana.lozi.vn hardware, whether or not such information is produced through the use of the Service.</p>
<h3>14. indemnification</h3>
<p>You agree to indemnify, hold harmless, and (at banana.lozi.vn's request) defend banana.lozi.vn and its officers, directors, shareholders, and representatives from and against any and all liability, damages, losses, costs, or expenses (including but not limited to attorneys' fees and expenses) incurred in connection with any claim related to (a) your breach of any term, condition, representation, warranty, or covenant in this Agreement; or (b) the information you supply to banana.lozi.vn or make available to any third party, including your registration data and the content of the web pages corresponding to the URLs you submit to banana.lozi.vn. This obligation shall survive any termination of your relationship with banana.lozi.vn.</p>
<h3>15. sole and exclusive remedy</h3>
<p>If you are dissatisfied with any portion of any Service, your sole and exclusive remedy is to cancel your account or your subscription to the given Service according to this Agreement. No refunds are available.</p>
<h3>16. general provisions</h3>
<p>The laws of the Netherland govern this agreement. You hereby consent to the jurisdiction of and venue in courts located in the Netherlands in all disputes arising out of or relating to this Agreement or your use of the Services. In addition, you hereby consent to the exclusive jurisdiction of, and venue in, such courts for any action commenced by you against banana.lozi.vn. The prevailing party in any dispute relating to the Services or this Agreement will be entitled to recover its costs, expenses and reasonable attorney fees incurred in connection with such dispute.</p>
<p>This Agreement, along with banana.lozi.vn's other terms, conditions, and policies referenced herein, constitutes the entire agreement between you and banana.lozi.vn with respect to the Services. To the extent of any conflict between this Agreement and banana.lozi.vn's Website Terms of Use, the Website Terms of Use will govern your general use of banana.lozi.vn's website, and this Agreement will govern your use of the Services in particular. If any part of this Agreement is held to be unenforceable, that part will be amended to achieve its intended effect as nearly as possible, and the remainder of the Agreement will remain in full force.</p>
<p>No party shall be liable for failure or delay in performing its obligations hereunder if such failure or delay is due to circumstances beyond the party's reasonable control, including without limitation, acts of any governmental body or failure of the software or equipment of third parties. Except as provided otherwise herein, any notice given under this Agreement will be made in writing by email and will be effective on the business day after it is sent. You may not assign this Agreement, and any attempt to do so is void. You acknowledge that your account is part of the banana.lozi.vn network, and, consequently, you will receive periodic announcements and information regarding banana.lozi.vn's services. You may request to be removed from the network's news mailing list at any time.</p>
<h3>17. publicity</h3>
<p>banana.lozi.vn may include your web site's domain name &amp; logo on its customer lists, testimonials and press releases. If at any time you would prefer not to be listed, simply let us know, and we will remove your name from the list.</p>
<h3>18. special remarks</h3>
<p>The following types of Web sites are NOT allowed to participate in the Service: (a) Web sites which promote illegal activity or racism, or which are libelous, defamatory, racist, hate-crime oriented, abusive, harassing or threatening, (b) Web sites which contain viruses or other contaminating or destructive features, (c) Web sites which violate the rights of others, such as Content which infringes any copyright, trademark, patent, trade secret or violates any right of privacy or publicity, (d) Web sites which provide instructions or discussions about performing illegal activities, (e) Web sites that promote or utilize software or services designed to deliver unsolicited email, or (f) Web sites which otherwise violate any applicable law or that we otherwise deem to be inappropriate.</p>
<p>You agree not to change the service's programming code.</p>
<p>You acknowledge and agree that general information about your Web site (name, URL, traffic counts, etc.) may be utilized by the Service. Possible uses include (but are not limited to) lists of the busiest sites, lists of member sites, general promotional uses, etc.</p>
<p>Opentracker stores its data in many different formats. 'Searchable data', is a particular format of data. Searchable data is used for the âuniversal searchâ feature and is kept for 60 days. The âuniversal searchâ box is located on the top of 'visitors online' report. The âuniversal searchâ box is experimental and may be removed or not function at any time. This will not effect all other reports.</p>
<h3>19. questions</h3>
<p>If you have any questions about this Agreement or banana.lozi.vn's Services, please contact banana.lozi.vn by email at&nbsp;<a href="mailto:coredumpgroup@gmail.com">coredumpgroup@gmail.com</a>.</p>
<p>Thank you for choosing banana.lozi.vn to me  asure your website.&nbsp;<br>
  We look forward to doing business with you and hope you find our services valuable.</p>


<br>
<br>
<br>
<?php include("app/layout/footer.php");?>
  </div>
</div>
<br>
<br>
<br>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>

</body>
