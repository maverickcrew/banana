<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isSignin($token);

?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>

<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </div>


<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="/public/img/img1.jpg" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="item active">
      <img src="/public/img/img1.jpg"  alt="First slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>Trắc nghiệm cuộc sống</h1>
          <p>Với những câu hỏi thú vị được lựa chọn một cách cẩn thận chúng tôi tin răng sẽ đem lại cho các bạn những kiến thức thú vị</p>
          <p><a class="btn btn-lg btn-primary" href="/register.php" role="button">Sign up today</a></p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="/public/img/img2.jpg" alt="Second slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>Trắc nghiệm maketing</h1>
          <p>Với lượng câu hỏi khổng lồ, bao phủ nhiều lĩnh vực kinh doanh được tổng hợp từ nhiều trường đại học, chắc chắn đây sẽ là nguồn tài liệu quan trọng cho các bạn sinh viên cũng như những ai quan tâm tới lĩnh vực maketing.</p>
          <p><a class="btn btn-lg btn-danger" href="/test.php" role="button">Bắt đầu</a></p>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="/public/img/img3.jpg" alt="Third slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>Đố vui</h1>
          <p>Vừa học vừa chơi với nhữngc câu hỏi không khó mà cũng không dễ :D.</p>
          <p><a class="btn btn-lg btn-success" href="/test.php" role="button">Thử thôi</a></p>
        </div>
      </div>
    </div>
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div><!-- /.carousel -->


<div class="container marketing">

  <!-- Three columns of text below the carousel -->
  <div class="row">
    <div class="col-lg-4">
      <img class="img-circle" src="public/image4.jpg" alt="Generic placeholder image">
      <h2>Le Thi Thu Duyen</h2>
      <p>This website helped me improve my English skill from beginer. That's awesome!!</p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
      <img class="img-circle" src="public/image3.jpg" alt="Generic placeholder image">
      <h2>Dao Nguyen Hoang Anh</h2>
      <p>I have got Toeci 890 after testing on this website. It's amazing. I can beleive it. Thank a lot!</p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
      <img class="img-circle" src="public/image2.jpg" alt="Generic placeholder image">
      <h2>Core Dump</h2>
      <p>This is a useful and activable environment for training. Everyday we spend many hours working together. This website has connected us.</p>
    </div><!-- /.col-lg-4 -->
  </div><!-- /.row -->


  <!-- START THE FEATURETTES -->

  <hr class="featurette-divider">

  <div class="row featurette">
    <div class="col-md-7">
      <h2 class="featurette-heading">English for computing. <br><span class="text-muted">It'll blow your mind.</span></h2>
      <p class="lead">Those test will improve your computing English skill. You will learn many vocabularies when making a test. Why don't you try it now?.</p>
      <p><a class="btn btn-default begin_test_btn" href="/test.php" role="button"><b>Begin test &raquo;</b></a></p>
    </div>
    <div class="col-md-5">
      <img class="featurette-image img-responsive" src="public/fixture/fixture1.jpg" alt="Generic placeholder image">
    </div>
  </div>

  <hr class="featurette-divider">

  <div class="row featurette">
    <div class="col-md-5">
      <img class="featurette-image img-responsive" src="public/fixture/fixture2.jpg" alt="Generic placeholder image">
    </div>
    <div class="col-md-7">
      <h2 class="featurette-heading">Using English at work! <br><span class="text-muted">See for yourself.</span></h2>
      <p class="lead">English is the most important skill at work. But how can I improve by myself?? The answer will be here. Let's check it out!.</p>
      <p><a class="btn btn-default begin_test_btn" href="/test.php" role="button"><b>Begin test &raquo;</b></a></p>
    </div>
  </div>

  <hr class="featurette-divider">

  <div class="row featurette">
    <div class="col-md-7">
      <h2 class="featurette-heading">English gammar. <br><span class="text-muted">Checkmate.</span></h2>
      <p class="lead">English grammar will give you more and more grammar questions so you can pratice it everyday. Grammar can help you write correctly and faster. You should do it now :D.</p>
      <p><a class="btn btn-default begin_test_btn" href="/test.php" role="button"><b>Begin test &raquo;</b></a></p>
    </div>
    <div class="col-md-5">
      <img class="featurette-image img-responsive" src="public/fixture/fixture3.jpg" alt="Generic placeholder image">
    </div>
  </div>

  <hr class="featurette-divider">

<?php include("app/layout/footer.php");?>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>
</body>

