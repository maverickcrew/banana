<?php
include_once "models/User.php";
include_once "models/Question.php";

$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
  header("Location: index.php");
  die();
}

$Question = new Question();
$unit = $_POST["unit"];
echo $unit;
if (!$unit) {
  header("Location: add_a_question.php");
  die();
}

$Question->insert($_POST);

header("Location: add_a_question.php");
die();

?>
