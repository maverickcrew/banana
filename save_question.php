<?php

include_once "models/User.php";
include_once "models/Question.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
    header("Location: index.php");
      die();
}
$Question = new Question();
$question_id = $_POST["id"];
echo $question_id;
$question = $Question->getOne($question_id);
if (!$question) {
  header("Location: question.php");
  die();
}

$question->content = $_POST["content"];
$question->caseA = $_POST["caseA"];
$question->caseB = $_POST["caseB"];
$question->caseC = $_POST["caseC"];
$question->caseD = $_POST["caseD"];
$question->answer = $_POST["answer"];

$Question->update($question);
header("Location: question.php");
die();

?>
