<?php

class Mysql {

  static protected $_connection;

  static public function open($host = "localhost", $port = "3306", $user = root, $pass = "", $name = "banana", $new_link = false) {
    if (!$new_link) {
      if (!isset(self::$_connection)) {
        self::$_connection = mysql_connect("$host:$port", $user, $pass) or die(mysql_error());
        mysql_select_db($name, self::$_connection);
        mysql_query('SET NAMES utf8', self::$_connection);
      }
      return self::$_connection;
    } else {
      $connection = mysql_connect("$host:$port", $user, $pass, $new_link) or die(mysql_error());
      mysql_select_db($name, $connection);
      mysql_query('SET NAMES utf8', $connection);
      return $connection;
    }
  }

  static public function close() {
    if (isset(self::$_connection)) {
      mysql_close(self::$_connection);
      self::$_connection = null;
    }
    return self::$_connection;
  }

}

?>
