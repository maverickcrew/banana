<?php

include_once "connection/Table.php";

class Category extends Table {

  public function count() {
    $select = $this->select();
    $result = $this->fetchOne($select->getCountQuery());
    return $result->{'COUNT(*)'};
  }

  public function getOne($id = null) {

    if ($id) {
      $select = $this->select(array("id", "occupation", "description", "image"))->where("`id` = '$id'");
      return $this->fetchOne($select);
    }
    return null;

  }

  public function getAll() {
    $select =  $this->select(array("id", "occupation", "description", "image"));
    return $this->fetchAll($select);
  }

  public function findByName($occupation = null) {
    if ($occupation) {
      $select = $this->select(array("id", "occupation", "description", "image"))->where("`occupation` = '$occupation'");
      return $this->fetchOne($select);
    }
    return null;
  }

}

?>
