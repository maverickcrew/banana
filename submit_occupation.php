<?php

include_once "models/Category.php";
include_once "models/User.php";

$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
  header("Location: index.php");
  die();
}

$Occupation = new Category();
$occupation = $_POST["occupation"];
if (!$occupation) {
  header("Location: add_an_occupation.php");
  die();
}

$Occupation->insert($_POST);

header("Location: question.php");
die();

?>
