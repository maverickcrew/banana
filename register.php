<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
if ($User->isSignin($token)) {
  header("Location: index.php");
  die();
}

?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>
<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <li><a href="/signin.php">Signin</a></li>
        </div>
      </div>
    </div>

   <form id="signupform" class="form-signup" role="form" method="post" action="/create.php">
       <h2 class="form-signin-heading text-center">Register</h2>
        <input name="name" type="text" class="form-control" placeholder="Name" required autofocus><br/>
        <input name="email" type="email" class="form-control" placeholder="Email address" required autofocus><br/>
        <input name="address"  type="text" class="form-control" placeholder="Address" required autofocus><br/>
        <input name="password" type="password" class="form-control" placeholder="Password" required>
        <label class="checkbox">
          <input id="policy" name="accept-condition" type="checkbox" value="remember-me"> Tôi đã đọc và đồng ý với các điều khoản dịch vụ của banana test.
       </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button><br/>
        <label id="errors"></label>
         <a href="/signin.php">Already have an account just signin!</a>
   </form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>

$(document).ready(function(){

$("#signupform").bind("submit", function(e){
  if (!$("#policy").is(":checked")) {
      $("#errors").show();
      $("#errors").text("Bạn phải đồng ý các điều khoản dịch vụ");
      e.preventDefault();
  } else  {
      $("#errors").hide();
  }

});

});

</script>

 </body>

