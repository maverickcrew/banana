<?php

include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isSignin($token);

if (!$user) {
  header("Location: signin.php");
  die();
}

$user->name = $_POST["name"];
$user->email = $_POST["email"];
$user->password = $_POST["password"];
$user->address = $_POST["address"];

$User->update($user);

header("Location: profile.php");
die();

?>
