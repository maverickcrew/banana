<?php

include_once "../models/Unit.php";

$Unit = new Unit();
$units = $Unit->findByOccupation($_GET["occupation"]);

?>

<option value="" selected="true">Please select one</option>
<?php foreach($units as $value): ?>
<option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
<?php endforeach; ?>
