<?php

include_once "models/User.php";

$User = new User();

$token = $_COOKIE["token"];
if ($token) {
  $user =  $User->select("name")->where("token=". $token);
  if (!$user) {
    header("Location: index.php");
    die();
  }
}

$_POST["role"] = "User";
$User->insert($_POST);
header("Location: signin.php");
die();

?>
