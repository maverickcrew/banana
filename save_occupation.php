<?php
include_once "models/Category.php";
include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
    header("Location: index.php");
      die();
}
$Category = new Category();
$category_id = $_POST["id"];
$category = $Category->getOne($category_id);
if (!$category) {
  header ("Location: question.php");
  die();
}

$category->occupation = $_POST["occupation"];
$category->description = $_POST["description"];
$category->image = $_POST["image"];

$Category->update($category);
header ("Location: question.php");

?>
