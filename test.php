<?php

include_once "models/User.php";

$User = new User();
$token = $_COOKIE["token"];
$user = $User->isSignin($token);

?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>

<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/users.php">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
             <input name="q" type="text" class="form-control" value="<?php echo $_GET["q"] ?>" placeholder="Search...">
          </form>
        </div>
      </div>
    </div>

    <div class="row placeholders">
    <?php
      include_once "models/Category.php";
      $Category = new Category();
      $categories = $Category->getAll();
      foreach ($categories as $value) {
    ?>
      <div class="col-xs-6 col-sm-3 placeholder cover-occupation">
           <img src="<?php echo $value->image ?>" class="img-responsive test_img" alt="Generic placeholder thumbnail"/>
           <h4><a href="/start.php?occupation=<?php echo $value->id ?>"><?php  echo $value->occupation ?></a></h4>
           <span class="text-muted"><?php echo $value->description?></span>
      </div>
    <?php } ?>
    </div>

    <?php include("app/layout/footer.php");?>

   </div><!-- /.container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>
</body>

