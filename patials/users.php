<?php

include_once "../models/User.php";

$User = new User();
$users = $User->getAllUser();

?>
<h1>Users</h1>
<table class="table table-striped">
    <thead>
        <tr>
            <th class="col-lg-1">No.</th>
            <th class="col-lg-4">Email</th>
            <th class="col-lg-2">Role</th>
        </tr>
    </thead>
    <tbody>

<?php foreach($users as $value): ?>

   <tr>
     <td><?php echo ++$i ?></td>
     <td><?php echo $value->email ?></td>
     <td>
       <?php echo $value->role ?>
       <a class="btn btn-primary btn-sm pull-right" type="button" href="/edit_user.php?id=<?php echo $value->id ?>">Edit</a>
     </td>
   </tr>

<?php endforeach; ?>
   </tbody>
</table>


