<?php
include_once "models/User.php";

$User = new User();
$token = $_COOKIE["token"];
$admin = $User->isAdmin($token);
if (!$admin) {
   header("Location: index.php");
   die();
}

$user = $User->getUserFromId($_POST["id"]);
if (!$user) {
  header ("Location: users.php");
  die();
}

$user->name = $_POST["name"];
$user->address = $_POST["address"];
$user->email = $_POST["email"];
$user->password = $_POST["password"];

$User->update($user);
header ("Location: users.php");
die();

?>
