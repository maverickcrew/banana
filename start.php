<?php

include_once "models/Unit.php";
include_once "models/Question.php";

$Unit = new Unit();
$Question = new Question();

$current = -1;
$occupation = $_GET["occupation"];
$unit  = $Unit->getRandomUnit($occupation)->id;
$total = $Question->countWithUnit($unit)-1;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="public/favicon.ico">
    <title>Banana testing online</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/start.css" rel="stylesheet">
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand"><a href="/test.php">Back to test center</a></h3>
            </div>
          </div>
          <div class="inner cover" id="askquestion">
          </div>
          <input id="current" type="hidden" value="<?php echo $current ?>">
          <input id="unit" type="hidden" value="<?php echo $unit ?>">
          <input id="answersheet" type="hidden" value="">
          <input id="total" type="hidden" value="<?php echo $total?>">
          <a href="/test.php" id="testcenter" style="display: none;"  class="btn btn-success btn-circle">TEST CENTER</a>
          <a href="#" id="next" class="btn btn-danger btn-circle">START</a>
          </p>

          <div class="mastfoot">
            <div class="inner">
              <p>Testing online <a href="#">Coredump</a>, by <a href="https://huyphams.github.com">huypham</a>.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/docs.min.js"></script>
<script>
$(document).ready(function () {

  $("#next").click(function () {
    $(this).attr("class","btn btn-default");
    $(this).text("Next");

    if ($("input[name=answer]:checked").val()){
      $("#answersheet").val( $("#answersheet").val()+"&answersheet[]="+ $("input[name=answer]:checked").val() );
    } else if (parseInt( $("#current").val()) >-1) {
      return;
    }

    if ($("#total").val()==$("#current").val()) {
      $(this).hide();
      $("#testcenter").show();
    }

    $("#current").val(parseInt($("#current").val())+1);
    $("#askquestion").load("/patials/askquestion.php?unit=" + $("#unit").val() + "&current=" + $("#current").val() + $("#answersheet").val() );
  });

});
</script>

  </body>
</html>

