<?php

include_once "connection/Table.php";

class Unit extends Table {

  public function count() {
    $select = $this->select();
    $result = $this->fetchOne($select->getCountQuery());
    return $result->{'COUNT(*)'};
  }

  public function getOne($id = null) {

    if ($id) {
      $select = $this->select(array("id", "occupation", "name"))->where("`id` = '$id'");
      return $this->fetchOne($select);
    }
    return null;

  }

  public function findByOccupation($occupation = null) {
    if ($occupation) {
      $select = $this->select(array("id","occupation", "name"))->where("`occupation` = $occupation");
      return $this->fetchAll($select);
    }
    return null;
  }

  public function getRandomUnit($occupation = null) {

      $select = $this->select(array("id", "name"))->where("`occupation` = $occupation");
      $results =  $this->fetchAll($select);
      $units = array();
      foreach ($results as $key) {
        array_push($units, $key);
      }
      $ran = rand(1, count($units));
      return $units[$ran-1];
    }

}

?>
