<?php

include_once "models/User.php";
include_once "models/Unit.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
  header("Location: signin.php");
  die();
}
$Unit = new Unit();
$unit = $Unit->getOne($_GET["id"]);
if (!$unit) {
  header ("Location: question.php");
  die();
}
?>

<?php include("app/layout/header.php");?>
<head>
 <link href="assets/css/signup.css" rel="stylesheet">
</head>
<body>
<div class="navbar-wrapper">
  <div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">BANANA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/test.php">TEST CENTER</a></li>
            <li><a href="/about.php">About</a></li>
            <li><a href="/contact.php">Contact</a></li>
            <?php if ($user): ?>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->name ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/">Users</a></li>
              <li><a href="/question.php">Question</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="/profile.php">Profile</a></li>
              <li><a href="/logout.php">Logout</a></li>
            </ul>
            </li>
           <?php else : ?>
            <li><a href="/signin.php">Sigin</a></li>
           <?php endif; ?>
          </ul>
          <form class="navbar-form navbar-right search-bar" method="get" action="/search.php">
            <input name="q" type="text" class="form-control" placeholder="Search...">
          </form>

        </div>
      </div>
    </div>


<form id="theform" class="form-signup" role="form" method="post" action="/save_unit.php">
   <h2 class="form-signin-heading text-center">Edit Unit</h2>
   <input type="hidden" name="id" value="<?php echo $unit->id ?>"></input><br/>
   <textarea id="name" name="name" class="form-control" rows="6" placeholder="Description"><?php echo $unit->name ?></textarea><br>
   <button class="btn btn-lg btn-primary col-lg-5" type="submit">Submit</button>
   <a href="/question.php" class="btn btn-lg btn-primary col-lg-5 pull-right">Cancel</a>
   <label id="nameerror"></label><br/>
</form>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

<?php include("app/layout/footer.php");?>
</div><!-- /.container -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>
<script>
$(document).ready(function(){

  $("#theform").bind("submit", function(e){
     if ($("#name").val()=="") {
        $("#nameerror").show();
        $("#nameerror").text("Occupation name không được bỏ trống");
        e.preventDefault();
     } else  {
        $("#nameerror").hide();
     }
  });

  $("#occupation").change(function(){
    $("#occupationid").val($(this).find(":selected").val());
  });
});

</script>
 </body>

