<?php

include_once "connection/Table.php";

class User extends Table {

  public function count() {
    $select = $this->select();
    $result = $this->fetchOne($select->getCountQuery());
    return $result->{'COUNT(*)'};
  }

  public function getOne($id = null) {

    if ($id) {
      $select = $this->select(array("id", "name", "email"))->where("`id` = '$id'");
      return $this->fetchOne($select);
    }
    return null;

  }

  public function getUserFromId($id = null) {

    if ($id) {
      $select = $this->select(array("id", "name", "email", "role", "address"))->where("`id` = '$id'");
      return $this->fetchOne($select);
    }
    return null;

  }


  public function getAll() {
    $select =  $this->select(array("id", "name", "role", "email", "token", "password"));
    return $this->fetchAll($select);
  }

  public function getAllUser() {
    $select = $this->select(array("id", "name", "role", "email", "token", "password"))->where("`role` <> 'Admin'");
    return $this->fetchAll($select);
  }




  public function findByEmail($email = null) {
    if ($email) {
      $select = $this->select(array("id", "name", "email", "token", "password"))->where("`email` = '$email'");
      return $this->fetchOne($select);
    }
    return null;
  }

  public function isSignin($token=null) {

    if ($token) {
      $select = $this->select(array("id", "name", "email", "token"))->where("`token` = '$token'");
      $user = $this->fetchOne($select);
      if ($user) {
        return $user;
      }
    }
    return false;
  }

  public function isAdmin($token=null) {

    if ($token) {
      $select = $this->select(array("id", "name", "email", "token", "role"))->where("`token` = '$token'");
      $user = $this->fetchOne($select);
      if ($user && $user->role == "Admin") {
        return $user;
      }
    }
    return false;
  }

}

?>
