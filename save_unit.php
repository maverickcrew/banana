<?php
include_once "models/Unit.php";
include_once "models/User.php";
$User = new User();
$token = $_COOKIE["token"];
$user = $User->isAdmin($token);
if (!$user) {
    header("Location: index.php");
      die();
}
$Unit = new Unit();
$unit_id = $_POST["id"];
$unit = $Unit->getOne($unit_id);
if (!$unit) {
  header ("Location: question.php");
  die();
}

$unit->name = $_POST["name"];

$Unit->update($unit);
header ("Location: question.php");
die()

?>
